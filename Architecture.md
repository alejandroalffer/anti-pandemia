# High level overview

```plantuml
!includeurl https://raw.githubusercontent.com/RicardoNiepel/C4-PlantUML/release/1-0/C4_Component.puml

LAYOUT_TOP_DOWN

Person(practitioner, "Practitioner", "Professional providing Health Certificates")
Person(citizen, "Citizen", "The user holding Health Certificates")
Person(otherPerson, "Other person", "A person receiving/verifying Health Certificate")
Container(healthAuthority, "Health Authority", "Technology", "Health authority managing pandemia")

Rel(citizen, healthAuthority, "Uses", "HTTPS")

Rel_R(practitioner, citizen, "Label", "Optional Technology")
Rel_R(citizen, otherPerson, "Uses", "HTTPS")

```
