## Citizen onboarding

The user downloads the mobile application and registers.
There is **not any interaction** with any external server, all activity is local to the mobile so the privacy is preserved.
The password is entered to safeguard the data in the mobile (eg. encrypting the data).
In particular, the pair of keys are generated locally in the mobile and never leave the device.

```plantuml
autonumber
skinparam sequenceArrowThickness 2
skinparam roundcorner 10
skinparam maxmessagesize 150

box "User" #LightCyan
  actor User as U
  participant "Mobile" as M
end box

U -> M: Download App

== Creation of DID ==
M -> U: Ask to generate DID
U -> M: Confirm
M -> M: Generate Private+Public keys
M -> M: Generate DID from keys

== Ask for personal data ==
M -> U: Ask for personal data & consent
U -> M: Provide name, address, mobile, ...

== Ask for GPS data ==
M -> U: Ask consent for GPS data
U -> M: Give consent (or not)

== Self-issue credential ==
M -> M: Create credential with personal data and consent
M -> M: Sign credential with private key
M -> U: Ask for new password 
U -> M: Provide new password (optionally fingerprint)
M -> M: Store data locally encrypted

```

After this process of onboarding, the citizen has stored in her mobile a self-issued credential which is not yet validated by anybody, so the trust on the included personal data is very low.
However, this data will be validated to some extent when a practitioner issues a health status credential, as shown in the following diagram.


## Receive Health Credential

In this interaction, the citizen is in physical contact with practitioner and receives a Health Status credential, exchanging QRs as the communication mechanism.

1. The user creates a self-issued credential and sends it to the practitioner
2. The practitioner checks personal data (name, address, mobile)
3. Practitioner creates health status credential and sends it to citizen
4. Citizen stores health status credential in her mobile

```plantuml
autonumber
skinparam sequenceArrowThickness 2
skinparam roundcorner 10
skinparam maxmessagesize 150

box "User" #LightCyan
  actor User as U
  participant "Mobile" as MU
end box

box "Practitioner" #LightCyan
  participant "Mobile" as MP
  actor Practitioner as P
end box

== Creation of Disposable Credential ==
U -> MU: Asks create Disposable Credential
MU -> MU: Derives Disposable DID from normal DID

MU -> MU: Get name, address, mobile, ...
MU -> MU: Create Disposable Credential

== Send credential as QR ==
MU -> MP: Display credential as QR
note over MU, MP #white
    <img:https://gitlab.com/hesusruiz/anti-pandemia/-/raw/master/qrcode.png {scale=0.2}> 
end note

MP -> MP: Scan Credential

== Validate personal data ==
MP -> P: Display citizen data
P -> MP: Confirm citizen data

== Create Health Status credential ==
MP -> P: Ask for Health Status
P -> MP: Input health data and confirm
MP -> MP: Generate Health Status credential

== Send credential as QR ==
MP -> MU: Display credential as QR
note over MU, MP #white
    <img:https://gitlab.com/hesusruiz/anti-pandemia/-/raw/master/qrcode.png {scale=0.2}> 
end note

MU -> MU: Scan Health credential

== Validate and store Health credential in mobile ==
MU -> U: Display Health credential data
U -> MU: Confirm

MU -> MU: Store Health credential encrypted
MU -> U: Display confirmation

```

## Display Health Credential

In this interaction the citizen can send a Health Status Credential to any other actor, who can verify the signature of the practitioner against the public registry of practitioner identities in the blockchain.
The credential can be sent to a practitioner, an official or another citizen.

```plantuml
autonumber
skinparam sequenceArrowThickness 2
skinparam roundcorner 10
skinparam maxmessagesize 150

box "User" #LightCyan
  actor User as U
  participant "Mobile" as MU
end box

box "Other person" #LightCyan
  participant "Mobile" as MP
  actor Person as P
end box

box "EBSI registry" #LightCyan
  Collections "Public Node" as BR
end box

== Send credential as QR ==
MU -> MP: Display credential as QR
note over MU, MP #white
    <img:https://gitlab.com/hesusruiz/anti-pandemia/-/raw/master/qrcode.png {scale=0.2}> 
end note

MP -> MP: Scan Credential

P -[hidden]> BR: This is hidden and used for separation

== Validate credential ==
MP -> BR: Validate practitioner signature

MP -> P: Display credential data

```

The process of validation of the practitioner signing the citizen's credential involves checking the signature against both the repository of signatures of practitioners and also the repository of entities attesting the identities of practitioners.
